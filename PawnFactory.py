# Abstract Pawn Factory
from abc import ABC, abstractmethod


class PawnFactory(ABC):

    @abstractmethod
    def create_pawn(self):
        pass
