from GameInstance import GameInstance

# Initializing the game

# Making sure an integer is entered
valid_input = False
while not valid_input:
    try:
        print("\nEnter the number of players:")
        player_count = input()
        if int(player_count) < 2 or int(player_count) > 4:
            raise ValueError
        GameInstance(int(player_count))
        valid_input = True
    except ValueError:
        print("Input must be an Integer between 2 and 4")
    except TypeError:
        print("Input must be an Integer between 2 and 4")

requirements = GameInstance.requirements
current_round = int()
active_player_id = int()
players_passed = int()
# Game ends when requirements runs out
# TODO make requirements not break everything when it doesn't have 7 cards left
while requirements.functionalRequirements:
    active_player_id = current_round % len(GameInstance.get_player_list())
    current_round += 1
    players_passed = 0
    print("Round: " + str(current_round))
    while players_passed < 4:
        # Updating the current player id in GameInstance to the current player and returning the player object to main.py
        GameInstance.set_current_player(active_player_id)
        active_player = GameInstance.get_current_player()

        # Displaying the current Player
        print("\nPlayer " + str(active_player_id + 1) + "'s turn")
        input("Press enter to continue...\n")
        active_player.to_string()

        # TESTING: Giving players lots of resources
        #  active_player.mod_design(10)
        #  active_player.mod_testing(10)
        #  active_player.mod_implementation(10)
        #  active_player.mod_documentation(10)
        #  active_player.mod_coffee(10)
        #  active_player.mod_knowledge(10)

        turn = None

        while turn != 0:
            turn = None
            print("Select an option (0 to end turn):")
            print("1. Place pawn on Coffee Machine")
            print("2. Place pawn on Design")
            print("3. Place pawn on Documentation")
            print("4. Place pawn on Implementation")
            print("5. Place pawn on Recruitment")
            print("6. Place pawn on Testing")
            print("7. Get functional requirement card")
            print("8. Get nonfunctional requirement card")
            print("9. Place pawn on Search Bar")
            # TODO come up with an actual name for getting coffee
            print("10. Place pawn on SunDoes")
            print("11. View your current cards")
            print("12. View Scoreboard")
            choice = input()
            print(choice)

            if choice == "1":  # Coffee Machine - Game Space 1
                pawn_placement = None
                while pawn_placement != 0:
                    print("Coffee Machine Space")
                    print("Assign a pawn to the Coffee Machine?:")
                    print("\nPawns: ")
                    active_player.display_pawns()
                    print("Y/n?")
                    pawn_choice = str(input())

                    # Assigns a Developer
                    if pawn_choice.capitalize() == "Y":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(1, [active_player.get_pawn(pawn_location)]):
                                # Returns the user to the main screen
                                pawn_placement = 0
                                active_player.get_pawn(pawn_location).location = "Coffee Machine"
                                active_player.get_pawn(pawn_location).location_id = 1
                                turn = 0

                        # A player has no available developers
                        else:
                            print("No available developers")
                            input("Press enter to continue...")
                            # Returns the user to the main screen
                            pawn_placement = 0

                    if pawn_choice.capitalize() == "N":
                        pawn_placement = 0

            if choice == "2":  # Design - Game Space 4
                pawn_placement = None
                while pawn_placement != 0:
                    print("Design Space")
                    print("Select from available pawns (0 to quit):")
                    print(active_player.display_pawns())
                    print("1. Scrum Master and a Developer")
                    print("2. Developer")
                    pawn_choice = input()

                    # Scrum Master and a Developer
                    if pawn_choice == "1":
                        # Checks if the player has an available developer and scrum master
                        if active_player.pawn_list[1].is_available() and active_player.idle_developers():
                            pawn_list = [active_player.get_pawn(GameInstance.select_first_developer(active_player_id)),
                                         active_player.get_pawn(1)]
                            if GameInstance.place_pawn(4, pawn_list):
                                for pawn in pawn_list:
                                    pawn.location = "Design"
                                    pawn.location_id = 4
                                turn = 0

                        elif not active_player.pawn_list[1].is_available():
                            print("Scrum master is unavailable")

                        elif not active_player.idle_developers():
                            print("No available Developers")
                            input("Press enter to continue...")
                            pawn_placement = 0

                    # Just a developer
                    if pawn_choice == "2":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(4, [active_player.get_pawn(pawn_location)]):
                                active_player.get_pawn(pawn_location).location = "Design"
                                active_player.get_pawn(pawn_location).location_id = 4
                                turn = 0
                        # A player has no available developers
                        else:
                            print("No available developers")
                    if pawn_choice == "0":
                        pawn_placement = 0

            if choice == "3":  # Documentation - Game Space 3
                pawn_placement = None
                while pawn_placement != 0:
                    print("Documentation Space")
                    print("Select from available pawns (0 to quit):")
                    print(active_player.display_pawns())
                    print("1. Scrum Master and a Developer")
                    print("2. Developer")
                    pawn_choice = input()

                    # Scrum Master and a Developer
                    if pawn_choice == "1":
                        # Checks if the player has an available developer and scrum master
                        if active_player.pawn_list[1].is_available() and active_player.idle_developers():
                            pawn_list = [active_player.get_pawn(GameInstance.select_first_developer(active_player_id)),
                                         active_player.get_pawn(1)]
                            if GameInstance.place_pawn(3, pawn_list):
                                for pawn in pawn_list:
                                    pawn.location = "Documentation"
                                    pawn.location_id = 3
                                turn = 0

                        elif not active_player.pawn_list[1].is_available():
                            print("Scrum master is unavailable")
                            input("Press enter to continue...")

                        elif not active_player.idle_developers():
                            print("No available Developers")
                            input("Press enter to continue...")
                            pawn_placement = 0

                    # Just a developer
                    if pawn_choice == "2":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(3, [active_player.get_pawn(pawn_location)]):
                                active_player.get_pawn(pawn_location).location = "Documentation"
                                active_player.get_pawn(pawn_location).location_id = 3
                                turn = 0
                        # A player has no available developers
                        else:
                            print("No available developers")
                            input("Press enter to continue...")
                            pawn_placement = 0
                    if pawn_choice == "0":
                        pawn_placement = 0

            if choice == "4":  # Implementation - Game Space 5
                pawn_placement = None
                while pawn_placement != 0:
                    print("Implementation Space")
                    print("Select from available pawns (0 to quit):")
                    print(active_player.display_pawns())
                    print("1. Scrum Master and a Developer")
                    print("2. Developer")
                    pawn_choice = input()

                    # Scrum Master and a Developer
                    if pawn_choice == "1":
                        # Checks if the player has an available developer and scrum master
                        if active_player.pawn_list[1].is_available() and active_player.idle_developers():
                            pawn_list = [active_player.get_pawn(GameInstance.select_first_developer(active_player_id)),
                                         active_player.get_pawn(1)]
                            if GameInstance.place_pawn(5, pawn_list):
                                for pawn in pawn_list:
                                    pawn.location = "Implementation"
                                    pawn.location_id = 5
                                turn = 0

                        elif not active_player.pawn_list[1].is_available():
                            print("Scrum master is unavailable")
                            input("Press enter to continue...")

                        elif not active_player.idle_developers():
                            print("No available Developers")
                            input("Press enter to continue...")
                            pawn_placement = 0

                    # Just a developer
                    if pawn_choice == "2":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(5, [active_player.get_pawn(pawn_location)]):
                                active_player.get_pawn(pawn_location).location = "Implementation"
                                active_player.get_pawn(pawn_location).location_id = 5
                                turn = 0
                        # A player has no available developers
                        else:
                            print("No available developers")
                            input("Press enter to continue...")
                            pawn_placement = 0
                    if pawn_choice == "0":
                        pawn_placement = 0

            if choice == "5":  # Recruitment - Game Space 0
                pawn_placement = None
                while pawn_placement != 0:
                    print("Recruitment Space")
                    print("Select from available pawns (0 to quit):")
                    print(active_player.display_pawns())
                    print("1. Product Owner and a Developer")
                    print("2. Two Developers")
                    pawn_choice = input()
                    # Product Owner and a Developer
                    if pawn_choice == "1":
                        # Checks if the player has an available developer and scrum master
                        if active_player.pawn_list[0].is_available() and active_player.idle_developers():
                            pawn_list = [active_player.get_pawn(GameInstance.select_first_developer(active_player_id)),
                                         active_player.get_pawn(0)]
                            if GameInstance.place_pawn(0, pawn_list):
                                for pawn in pawn_list:
                                    pawn.location = "Recruitment"
                                    pawn.location_id = 0
                                turn = 0

                        elif not active_player.pawn_list[0].is_available():
                            print("Product Owner is unavailable")
                            input("Press enter to continue...")

                        elif not active_player.idle_developers():
                            print("No available Developers")
                            input("Press enter to continue...")
                            pawn_placement = 0

                    if pawn_choice == "2":
                        # Checks if the player has 2 idle developers
                        if len(active_player.idle_developers()) >= 2:
                            # Since this requires two pawns before setting their status, select_first_developer does not work

                            pawn_list = [active_player.get_pawn(active_player.idle_developers()[0]),
                                         active_player.get_pawn(active_player.idle_developers()[1])]

                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(0, pawn_list):
                                for pawn in pawn_list:
                                    pawn.location = "Recruitment"
                                    pawn.location_id = 0
                                turn = 0

                        # A player has no available developers
                        else:
                            print("Not enough available developers")
                            input("Press enter to continue...")
                            pawn_placement = 0
                    if pawn_choice == "0":
                        pawn_placement = 0

            if choice == "6":  # Testing - Game Space 6
                pawn_placement = None
                while pawn_placement != 0:
                    print("Testing Space")
                    print("Select from available pawns (0 to quit):")
                    print(active_player.display_pawns())
                    print("1. Scrum Master and a Developer")
                    print("2. Developer")
                    pawn_choice = input()

                    # Scrum Master and a Developer
                    if pawn_choice == "1":
                        # Checks if the player has an available developer and scrum master
                        if active_player.pawn_list[1].is_available() and active_player.idle_developers():
                            pawn_list = [active_player.get_pawn(GameInstance.select_first_developer(active_player_id)),
                                         active_player.get_pawn(1)]
                            if GameInstance.place_pawn(6, pawn_list):
                                for pawn in pawn_list:
                                    pawn.location = "Testing"
                                    pawn.location_id = 6
                                turn = 0

                        elif not active_player.pawn_list[1].is_available():
                            print("Scrum master is unavailable")
                            input("Press enter to continue...")

                        elif not active_player.idle_developers():
                            print("No available Developers")
                            input("Press enter to continue...")
                            pawn_placement = 0

                    # Just a developer
                    if pawn_choice == "2":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(6, [active_player.get_pawn(pawn_location)]):
                                active_player.get_pawn(pawn_location).location = "Testing"
                                active_player.get_pawn(pawn_location).location_id = 6
                                turn = 0

                        # A player has no available developers
                        else:
                            print("No available developers")
                            input("Press enter to continue...")
                            pawn_placement = 0

                    if pawn_choice == "0":
                        pawn_placement = 0

            # TODO use game spaces for this
            if choice == "7":  # Get functional card - Game Space  12
                move = None
                print("Your resources:")
                active_player.to_string()
                while move != 0:
                    requirements.only_top_seven_functional()
                    print("Select a card ID (0 to quit):")
                    card_id = input()
                    if card_id == "0":
                        break
                    else:
                        if active_player.is_obtainable_functional(card_id, requirements):
                            # Mod all player's resources
                            active_player.mod_design(
                                int(active_player.design) - int(requirements.get_card_functional(card_id).designReq))
                            active_player.mod_testing(
                                int(active_player.testing) - int(requirements.get_card_functional(card_id).testReq))
                            active_player.mod_documentation(
                                int(active_player.documentation) - int(requirements.get_card_functional(card_id).docReq))
                            active_player.mod_implementation(int(active_player.implementation) - int(
                                requirements.get_card_functional(card_id).impReq))
                            active_player.add_functional_card(card_id, requirements)
                            move = 0
                            print("\nGained the card.\n")
                            turn = 0
                        else:
                            print("\nNot enough resources.\n")

            # TODO use game spaces for this
            if choice == "8":  # Get nonfunctional card - Game Space  8-11
                move = None
                print("Your resources:")
                active_player.to_string()
                while move != 0:
                    requirements.only_top_five_nonfunctional()
                    print("Select a card ID (0 to quit):")
                    card_id = input()
                    if card_id == "0":
                        break
                    else:
                        if active_player.is_obtainable_nonfunctional(card_id, requirements):
                            # Mod all player's resources
                            active_player.mod_design(
                                int(active_player.design) - int(requirements.get_card_nonfunctional(card_id).designReq))
                            active_player.mod_testing(
                                int(active_player.testing) - int(requirements.get_card_nonfunctional(card_id).testReq))
                            active_player.mod_documentation(
                                int(active_player.documentation) - int(requirements.get_card_nonfunctional(card_id).docReq))
                            active_player.mod_implementation(int(active_player.implementation) - int(
                                requirements.get_card_nonfunctional(card_id).impReq))
                            active_player.add_nonfunctional_card(card_id, requirements)
                            move = 0
                            print("\nGained the card.\n")
                            turn = 0
                        else:
                            print("\nNot enough resources.\n")

            if choice == "9":  # Search Bar - Game Space 2
                pawn_placement = None
                while pawn_placement != 0:
                    print("Search Bar")
                    print("Assign a pawn to the Search Bar?:")
                    print("\nPawns: ")
                    active_player.display_pawns()
                    print("Y/n?")
                    pawn_choice = str(input())

                    # Assigns a Developer
                    if pawn_choice.capitalize() == "Y":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            if GameInstance.place_pawn(2, [active_player.get_pawn(pawn_location)]):
                                # Returns the user to the main screen
                                pawn_placement = 0
                                active_player.get_pawn(pawn_location).location = "Search Bar"
                                active_player.get_pawn(pawn_location).location_id = 2
                                turn = 0

                        # A player has no available developers
                        else:
                            print("No available developers")
                            input("Press enter to continue...")
                            # Returns the user to the main screen
                            pawn_placement = 0

                    if pawn_choice.capitalize() == "N":
                        pawn_placement = 0

            if choice == "10":  # SunDoes - Game Space 7
                pawn_placement = None
                while pawn_placement != 0:
                    print("SunDoes")
                    print("Assign a pawn to SunDoes?:")
                    print("\nPawns: ")
                    active_player.display_pawns()
                    print("Y/n?")
                    pawn_choice = str(input())

                    # Assigns a Developer
                    if pawn_choice.capitalize() == "Y":
                        # Checks if the player has idle developers
                        if active_player.idle_developers():
                            pawn_location = GameInstance.select_first_developer(active_player_id)
                            # Game spaces requires an array of pawns
                            GameInstance.place_pawn(7, [active_player.get_pawn(pawn_location)])
                            # Returns the user to the main screen
                            active_player.get_pawn(pawn_location).location = "SunDoes"
                            active_player.get_pawn(pawn_location).location_id = 7
                            turn = 0

                        # A player has no available developers
                        else:
                            print("No available developers")
                            input("Press enter to continue...")
                            # Returns the user to the main screen
                            pawn_placement = 0

                    if pawn_choice.capitalize() == "N":
                        pawn_placement = 0

            if choice == "11":  # Display current player cards
                active_player.display_deck()
                input("Press Enter to continue...")

            if choice == "12":  # Display scoreboard
                for y in range(len(GameInstance.get_player_list())):
                    print("Player " + str(y) + ": " + str(active_player.points))
                input("Press Enter to continue...")

            if choice == "0":  # End Turn
                turn = 0

            if choice == "13":
                requirements.functionalRequirements = []

        while players_passed < 4:
            active_player_id += 1
            if active_player_id >= len(GameInstance.get_player_list()):
                active_player_id -= len(GameInstance.get_player_list())
            GameInstance.set_current_player(active_player_id)
            active_player = GameInstance.get_current_player()
            if active_player.idle_developers():
                players_passed = 0
                break
            players_passed += 1


    # End of round cleanup
    GameInstance.end_round()
