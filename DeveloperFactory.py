from PawnFactory import PawnFactory
from Developer import Developer
"""
    @Name: DeveloperFactory
    @Author: Michael Bolles
"""


class DeveloperFactory(PawnFactory):

    # Factory method to create a Product Owner Pawn
    @staticmethod
    def create_pawn(player_id):
        return Developer(player_id)
