from GameSpace import GameSpace
from ScrumMaster import ScrumMaster
from PlayerList import PlayerList


class RecruitSpace(GameSpace):
    def canPlace(self, list_pawns):
        if len(self.pawns) != 0:
            return False
        if len(list_pawns) != 2:
            return False
        for pawn in list_pawns:
            if isinstance(pawn, ScrumMaster):
                return False
        return True

    def collectPawns(self, player_num):
        PlayerList.get_player(player_num).add_pawn()
        self.pawns.clear()
