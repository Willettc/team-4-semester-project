from abc import ABC


class Pawn(ABC):

    def __init__(self, player_id):
        self.player = player_id
        self.status = True
        # If a pawn is placed, this is set to true.
        # This is to differentiate pawns that are unavailable because they were not fed last round from placed pawns
        self.placed = False
        self.location = str()
        self.location_id = int()

    def is_idle(self):
        return self.status

    def is_placed(self):
        return self.placed

    # Checks if a pawn is both available and not placed
    def is_available(self):
        if self.status and not self.placed:
            return True
        else:
            return False

    # Resets the pawn's placement status back to the original state
    def reset_position(self):
        self.placed = False
        self.location = str()
        self.location_id = int()

    def __str__(self):
        # Obtains what type of pawn it is
        string = str(type(self)).split('.')[1][:-2] + " Status: "
        # Pawn is placed at a location and unavailable
        if self.placed and not self.status:
            string += "At " + self.location
        # Pawn was not fed the previous round
        elif not self.placed and not self.status:
            string += "Decaffinated"
        # Pawn is available
        else:
            string += "Available"
        return string
