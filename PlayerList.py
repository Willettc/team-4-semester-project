from Player import Player
"""
    @Name: PlayerList
    @Description: PlayerList holds an internal list of players for use in GameInstance
    @Author: Michael Bolles
"""


class PlayerList(object):
    players = []

    @staticmethod
    def __init__(num_players):
        # This is for unit tests because it breaks everything without it in those, otherwise its fine
        PlayerList.players.clear()
        for x in range(num_players):
            PlayerList.players.append(Player(x))

    @staticmethod
    def __str__():
        for y in PlayerList.players:
            print(y)

    # Returns number of players
    @staticmethod
    def num_players():
        return len(PlayerList.players)

    # Returns the player that is located at index id
    @staticmethod
    def get_player(player_id):
        return PlayerList.players[player_id]

    # Returns the list of players
    @staticmethod
    def get_player_list():
        return PlayerList.players
