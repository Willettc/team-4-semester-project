# Requirements class - Simon Cichonski
import sys
sys.path.append('/FunctionalRequirement.py')
sys.path.append('/NonfunctionalRequirement.py')
import FunctionalRequirement
import NonfunctionalRequirement
import json
import random


class Requirements:
    # functionalRequirements = []
    # nonfunctionalRequirements = []
    # Create decks of cards
    def __init__(self):
        self.functionalRequirements = []
        self.nonfunctionalRequirements = []
        """
        # Create decks of cards
        # self.obj = Requirements()
        # self.obj.populate_requirements()  # Populates the lists above with the requirements.json file
        # Shuffle the lists of requirements
        # random.shuffle(self.obj.functionalRequirements)
        # random.shuffle(self.obj.nonfunctionalRequirements)
        """

    # Return the data contained in requirements.json
    def open_json(self):
        with open('requirements.json', 'r') as f:
            data = json.load(f)

        return data

    # Use data to populate both functional and nonfunctional lists
    def populate_requirements(self):
        data = self.open_json()
        # Gathering each JSON object in 'functional'
        for i in data['functional']:
            # Appends a new Functional Requirement to the list
            self.create_functional(str(i['id']), str(i['name']), str(i['designReq']), str(i['testReq']), str(i['impReq']),
                                   str(i['docReq']), str(i['points']))

        # Gathering each JSON object in 'nonfunctional'
        for i in data['nonfunctional']:
            # Appends a new Nonfunctional Requirement to the list
            self.create_nonfunctional(str(i['id']), str(i['name']), str(i['designReq']), str(i['testReq']), str(i['impReq']),
                                      str(i['docReq']), str(i['points']), str(i['resource']), str(i['numResource']))

    # create a Functional Requirement and add it to the list of Functional Requirements
    def create_functional(self, id, name, designreq, testreq, impreq, docreq, points):
        functional_requirement = FunctionalRequirement.Functional(id, name, designreq, testreq, impreq, docreq, points)
        self.functionalRequirements.append(functional_requirement)

    # create Nonfunctional Requirement and add it to the list of Functional Requirements
    def create_nonfunctional(self, id, name, designreq, testreq, impreq, docreq, points, resource, numresource):
        nonfunctional_requirement = NonfunctionalRequirement.Nonfunctional(id, name, designreq, testreq,
                                                                           impreq, docreq, points, resource,
                                                                           numresource)
        self.nonfunctionalRequirements.append(nonfunctional_requirement)

    # only return the top seven functional cards
    def only_top_seven_functional(self):
        seven_functional_requirements = []
        # Appends the top 7 cards to the list above
        for x in range(0, 7):
            try:
                seven_functional_requirements.append(self.functionalRequirements[x])
            except:
                pass
        for y in range(len(seven_functional_requirements)):
            print()
            print("ID: " + seven_functional_requirements[y].id)
            print("NAME: " + seven_functional_requirements[y].name)
            print("DESIGN: " + seven_functional_requirements[y].designReq)
            print("TESTING: " + seven_functional_requirements[y].testReq)
            print("IMPLEMENTATION: " + seven_functional_requirements[y].impReq)
            print("DOCUMENTATION: " + seven_functional_requirements[y].docReq)
            print("POINTS: " + seven_functional_requirements[y].points)

    def only_top_five_nonfunctional(self):
        five_nonfunctional_requirements = []
        # Appends the top 5 cards to the list above
        for x in range(0, 5):
            try:
                five_nonfunctional_requirements.append(self.nonfunctionalRequirements[x])
            except:
                pass
        for y in range(len(five_nonfunctional_requirements)):
            print()
            print("ID: " + five_nonfunctional_requirements[y].id)
            print("NAME: " + five_nonfunctional_requirements[y].name)
            print("DESIGN: " + five_nonfunctional_requirements[y].designReq)
            print("TESTING: " + five_nonfunctional_requirements[y].testReq)
            print("IMPLEMENTATION: " + five_nonfunctional_requirements[y].impReq)
            print("DOCUMENTATION: " + five_nonfunctional_requirements[y].docReq)
            print("POINTS: " + five_nonfunctional_requirements[y].points)

    # These methods will delete cards from the list
    def delete_functional_card(self, card_id):
        card_index = None
        for x in range(len(self.functionalRequirements)):
            # When the id passed matches the id of the card, move the card from the list
            if self.functionalRequirements[x].id == card_id:
                card_index = x
        del self.functionalRequirements[card_index]

    def delete_nonfunctional_card(self, card_id):
        card_index = None
        for x in range(len(self.nonfunctionalRequirements)):
            if self.nonfunctionalRequirements[x].id == card_id:
                card_index = x
        del self.nonfunctionalRequirements[card_index]

    def get_card_functional(self, id):
        id = str(id)
        for x in range(len(self.functionalRequirements)):
            if id == self.functionalRequirements[x].id:
                return self.functionalRequirements[x]

    def get_card_nonfunctional(self, id):
        id = str(id)
        for x in range(len(self.nonfunctionalRequirements)):
            if id == self.nonfunctionalRequirements[x].id:
                return self.nonfunctionalRequirements[x]

    # Display cards to console
    def display_deck(self):
        print("Functional Requirements Deck")
        for x in range(len(self.functionalRequirements)):
            print()
            print("ID: " + self.functionalRequirements[x].id)
            print("NAME: " + self.functionalRequirements[x].name)
            print("DESIGN: " + self.functionalRequirements[x].designReq)
            print("TESTING: " + self.functionalRequirements[x].testReq)
            print("IMPLEMENTATION: " + self.functionalRequirements[x].impReq)
            print("DOCUMENTATION: " + self.functionalRequirements[x].docReq)
            print("POINTS: " + self.functionalRequirements[x].points)
            print()

        print("Nonfunctional Requirements Deck")
        for x in range(len(self.nonfunctionalRequirements)):
            print()
            print("ID: " + self.nonfunctionalRequirements[x].id)
            print("NAME:" + self.nonfunctionalRequirements[x].name)
            print("DESIGN: " + self.nonfunctionalRequirements[x].designReq)
            print("TESTING: " + self.nonfunctionalRequirements[x].testReq)
            print("IMPLEMENTATION: " + self.nonfunctionalRequirements[x].impReq)
            print("DOCUMENTATION: " + self.nonfunctionalRequirements[x].docReq)
            print("RESOURCE: " + self.nonfunctionalRequirements[x].resource)
            print("NUMBER: " + self.nonfunctionalRequirements[x].numResource)
            print("POINTS: " + self.nonfunctionalRequirements[x].points)
            print()



"""
obj = Requirements()
obj.populate_requirements()  # Populates the lists above with the requirements.json file
# Shuffle the lists of requirements
random.shuffle(obj.functionalRequirements)
random.shuffle(obj.nonfunctionalRequirements)
obj.display_deck()
"""
