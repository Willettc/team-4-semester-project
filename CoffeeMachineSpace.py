from GameSpace import GameSpace
from Developer import Developer
from PlayerList import PlayerList


class CoffeeMachineSpace(GameSpace):
    def canPlace(self, list_pawns):
        if len(self.pawns) != 0:
            return False
        if len(list_pawns) != 1:
            return False
        if not isinstance(list_pawns[0], Developer):
            return False
        return True

    def collectPawns(self, player_num):
        PlayerList.get_player(player_num).coffee_level += 1
        self.pawns.clear()
