from GameSpaceList import GameSpaceList
from PlayerList import PlayerList
from Requirements import Requirements
import random
import math


class GameInstance:
    current_player = int()
    current_phase = int()
    requirements = []

    @staticmethod
    def __init__(player_count):
        # Generate the Players
        PlayerList(player_count)
        # Generate the game space list
        GameSpaceList.init()
        # Generate the  requirement deck
        GameInstance.requirements = Requirements()
        Requirements.populate_requirements(GameInstance.requirements)
        random.shuffle(GameInstance.requirements.functionalRequirements)
        random.shuffle(GameInstance.requirements.nonfunctionalRequirements)
        # TODO Add anything else that needs to be initialized

    # Returns the current player as an object
    @staticmethod
    def get_current_player():
        return PlayerList.get_player(GameInstance.current_player)

    # Returns an integer related to the current player's index location
    @staticmethod
    def get_current_player_id():
        return GameInstance.current_player

    @staticmethod
    def get_current_phase():
        return GameInstance.current_phase

    # Returns the player list
    @staticmethod
    def get_player_list():
        return PlayerList.get_player_list()

    # Sets the current player ID
    @staticmethod
    def set_current_player(current_player_id):
        GameInstance.current_player = current_player_id

    @staticmethod
    def set_current_phase(current_phase_id):
        GameInstance.current_phase = current_phase_id

    """
            @Name place_pawn
            @Description: Attempts to place the provided pawns onto the provided game space, throwing an error if unable
            @Author: Michael Bolles
    """

    @staticmethod
    def place_pawn(game_space, list_pawns):
        try:
            GameSpaceList.addPawns(game_space, list_pawns)
            # Checking if the pawn actually got placed, if not throw an exception so the console knows it didn't work
            if all(item in GameSpaceList.GameSpaces[game_space].pawns for item in list_pawns):
                for pawn in list_pawns:
                    pawn.placed = True
                    pawn.status = False
                print("Pawns placed\n")
                input("Press enter to continue...")
                return True
            else:
                raise ValueError

        except ValueError:
            print("Pawns can not be placed, most likely due to the space being full")
            input("Press enter to continue...")
            return False

    """
        @Name select_first_developer
        @Description: Runs player's idle_developer() and selects the first available developer from the list of indices
        @Author: Michael Bolles
    """

    @staticmethod
    def select_first_developer(player_id):
        available_pawns = PlayerList.get_player(player_id).idle_developers()
        return available_pawns[0]

    """
        @Name: feed_pawn
        @Description: Spends coffee for each pawn a player owns, to be called at the end of a round AFTER pawns are reclaimed
        @Author: Michael Bolles
    """

    @staticmethod
    def feed_pawn(pawn):
        player = PlayerList.get_player(pawn.player)
        # If a pawn is placed, and the player has coffee, the pawn is fed and the status is set to true
        if pawn.is_idle() is False and pawn.is_placed() is True and player.coffee > 0:
            player.mod_coffee(-1)
            pawn.status = True
        # If a pawn is placed, and the player does not have enough coffee, the pawn's status remains false
        elif pawn.is_idle() is False and pawn.is_placed() is True and player.coffee <= 0:
            pawn.status = False
        # If a pawn's status is false but it is not placed, it is returned to being placeable
        elif pawn.is_idle() is False and pawn.is_placed() is False:
            pawn.status = True
        pawn.reset_position()

    """
        @Name: adjust_die
        @Description: Prompts the user to spend knowledge to improve a die roll
        @Author: Michael Bolles
    """
    @staticmethod
    def adjust_die(die_roll, resource_type, divider, player_id):
        player = PlayerList.get_player(player_id)
        if player.knowledge > player.knowledge_used:
            available_knowledge = player.knowledge + player.knowledge_used
            valid_input = False
            while not valid_input:
                try:
                    print("Player: " + str(player_id + 1))
                    print("Roll: " + str(die_roll) + " Resource Type: " + str(resource_type) + " Available knowledge: " +
                          str(available_knowledge))
                    print("To get another copy of this resource, the roll needs to be a multiple of: " + str(divider))
                    print("How much do you want to use to improve this roll?")
                    amount = input()
                    if int(amount) > available_knowledge:
                        raise ValueError("Amount must be an integer equal to or less than available knowledge")
                    elif int(amount) < 0:
                        raise ValueError("Must be a positive integer")
                    elif int(amount) == 0:
                        valid_input = True
                    else:
                        player.mod_knowledge_used(int(amount) * -1)
                        valid_input = True
                        new_die_roll = int(die_roll) + int(amount)
                        print(new_die_roll)
                        return int(new_die_roll)
                except ValueError as v:
                    print(v)
                except TypeError:
                    print("Amount must be an integer")
        # If the player doesn't have any spare knowledge, do nothing
        else:
            return int(die_roll)

    """
        @Name: reclaim_pawn
        @Description: Reclaims each pawn, giving appropriate resource for each space
        @Author: Michael Bolles
    """

    @staticmethod
    def reclaim_pawn():
        for player in PlayerList.get_player_list():
            for pawn in player.get_pawn_list():
                if pawn.is_placed():
                    if GameSpaceList.canCollectPawns(pawn.location_id, pawn.player):
                        GameSpaceList.collectPawns(pawn.location_id, pawn.player)
                        if 2 < int(pawn.location_id) < 8:
                            result = GameSpaceList.GameSpaces[pawn.location_id].DieResult
                            # Documentation
                            if pawn.location_id == 3:
                                result = GameInstance.adjust_die(result, "Documentation", 3, pawn.player)
                                result = math.floor(result / 3)
                                PlayerList.get_player(pawn.player).mod_documentation(int(result))

                            # Design
                            if pawn.location_id == 4:
                                result = GameInstance.adjust_die(result, "Design", 4, pawn.player)
                                result = math.floor(result / 4)
                                PlayerList.get_player(pawn.player).mod_design(int(result))

                            # Implementation
                            if pawn.location_id == 5:
                                result = GameInstance.adjust_die(result, "Implementation", 5, pawn.player)
                                result = math.floor(result / 5)
                                PlayerList.get_player(pawn.player).mod_implementation(int(result))

                            # Testing resource
                            if pawn.location_id == 6:
                                result = GameInstance.adjust_die(result, "Testing", 6, pawn.player)
                                result = math.floor(result / 6)
                                PlayerList.get_player(pawn.player).mod_testing(int(result))

                            # Coffee
                            if pawn.location_id == 7:
                                result = GameInstance.adjust_die(result, "Coffee", 2, pawn.player)
                                result = math.floor(result / 2)
                                PlayerList.get_player(pawn.player).mod_coffee(int(result))
                        # end if 2< int
                    # end if GameSpaceList
                # end if pawn.is_placed
            # end for pawn
        # end for player

    """
         @Name: end_round
        @Description: Ends the round, resetting the board back to empty and running feed_pawn and reclaim_pawn
        @Author: Michael Bolles
    """

    @staticmethod
    def end_round():
        GameInstance.reclaim_pawn()
        for player in PlayerList.get_player_list():
            # Giving each player coffee equal to their coffee level
            player.mod_coffee(player.coffee_level)
            for pawn in player.pawn_list:
                GameInstance.feed_pawn(pawn)
