import unittest
from Requirements import Requirements


class MyTestCase(unittest.TestCase):

    def test_deck(self):
        # a deck should be instantiatable
        deck = Requirements()
        Requirements.populate_requirements(deck)
        self.assertNotEqual(deck, [])

    def test_functionalRequirements(self):
        # the deck should contain all 9 requirement.json functional requirements and start with id "01"
        deck = Requirements()
        Requirements.populate_requirements(deck)
        self.assertEqual(len(deck.functionalRequirements), 9)
        self.assertEqual(deck.functionalRequirements[0].id, "01")

    def test_nonFunctionalRequirements(self):
        # the deck should contain all 10 requirement.json nonfunctional requirements and start with id "01"
        deck = Requirements()
        Requirements.populate_requirements(deck)
        self.assertEqual(len(deck.nonfunctionalRequirements), 10)
        self.assertEqual(deck.nonfunctionalRequirements[0].id, "01")

    def test_card_deletion(self):
        # a deck should allow selected cards to be deleted by id and removed from the deck
        deck = Requirements()
        Requirements.populate_requirements(deck)
        self.assertEqual(len(deck.functionalRequirements), 9)
        self.assertEqual(len(deck.nonfunctionalRequirements), 10)
        deck.delete_nonfunctional_card("10")
        deck.delete_functional_card("09")
        self.assertEqual(len(deck.functionalRequirements), 8)
        self.assertEqual(len(deck.nonfunctionalRequirements), 9)


if __name__ == '__main__':
    unittest.main()
