from RequirementSpace import RequirementSpace
from PlayerList import PlayerList


class FunctionalRequirementSpace(RequirementSpace):
    cardsLeft: object
    def __init__(self):
        super().__init__()
        self.cardsLeft = 7

    def collectPawns(self, player_num):
        if (
            int(self.card.designReq) <= PlayerList.get_player(player_num).design
            and int(self.card.testReq) <= PlayerList.get_player(player_num).testing
            and int(self.card.impReq) <= PlayerList.get_player(player_num).implementation
            and int(self.card.docReq) <= PlayerList.get_player(player_num).documentation
        ):
            PlayerList.get_player(player_num).design -= int(self.card.designReq)
            PlayerList.get_player(player_num).testing -= int(self.card.testReq)
            PlayerList.get_player(player_num).implementation -= int(self.card.impReq)
            PlayerList.get_player(player_num).documentation -= int(self.card.docReq)
            PlayerList.get_player(player_num).functional_req.append(self.card)
            self.card = None
            self.cardsLeft -= 1
        self.pawns.clear()
