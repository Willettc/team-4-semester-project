class GameSpace:
    pawns: list

    DieResult = 0
    SpaceUsed = None

    def __init__(self):
        self.pawns = []

    def canPlace(self, list_pawns):
        pass

    def addPawns(self, list_pawns):
        if self.canPlace(list_pawns):
            for pawn in list_pawns:
                self.pawns.append(pawn)

    def canCollectPawns(self, player_num):
        for pawn in self.pawns:
            if pawn.player == player_num:
                return True
        return False

    def collectPawns(self, player_num):
        pass
