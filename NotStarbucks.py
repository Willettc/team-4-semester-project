from ResourceSpace import ResourceSpace
from ProductOwner import ProductOwner
from ScrumMaster import ScrumMaster


class NotStarbucks(ResourceSpace):
    def __init__(self):
        super().__init__(2, 2)

    def canPlace(self, list_pawns):
        for pawn in list_pawns:
            if isinstance(pawn, (ProductOwner, ScrumMaster)):
                return False
        return True
