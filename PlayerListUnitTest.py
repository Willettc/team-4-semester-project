from PlayerList import PlayerList
import unittest


class PlayerListUnitTestCase(unittest.TestCase):
    player_count = 4
    # PlayerList is static and needs to be declared outside of methods that aren't initializers
    PlayerList(player_count)

    def test_PlayerList_initialization(self):
        # PlayerList should make a list of players equal to the number inputted in it's initialization

        print(len(PlayerList.players))
        self.assertEqual(len(PlayerList.players), self.player_count)

    def test_PlayerList_num_players(self):
        # PlayerList.num_players should return the number of players.
        self.assertEqual(PlayerList.num_players(), self.player_count)

    def test_PlayerList_get_player(self):
        # PlayerList.get_player(player_id) should return the player located at the index location with the same id
        player_id = 3
        print(PlayerList.players)
        self.assertEqual(PlayerList.get_player(player_id), PlayerList.players[player_id])

    def test_PlayerList_get_player_list(self):
        # Returns the players attribute stored in PlayerList
        self.assertEqual(PlayerList.get_player_list(), PlayerList.players)


if __name__ == '__main__':
    unittest.main()
