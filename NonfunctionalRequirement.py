class Nonfunctional:
    # Constructor method with nonfunctional requirement variables
    def __init__(self, id, name, designreq, testreq, impreq, docreq, points, resource, numresource):
        self.id = id
        self.name = name
        self.designReq = designreq
        self.testReq = testreq
        self.impReq = impreq
        self.docReq = docreq
        self.points = points
        self.resource = resource
        self.numResource = numresource