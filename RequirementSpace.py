from GameSpace import GameSpace
from Developer import Developer
from ProductOwner import ProductOwner


class RequirementSpace(GameSpace):
    card: object

    def __init__(self):
        super().__init__()
        self.card = None

    def canPlace(self, list_pawns):
        if len(self.pawns) != 0:
            return False
        if len(list_pawns) != 2:
            return False
        if isinstance(list_pawns[0], Developer) and isinstance(list_pawns[1], ProductOwner):
            return True
        if isinstance(list_pawns[1], Developer) and isinstance(list_pawns[0], ProductOwner):
            return True
        return False
