from PawnFactory import PawnFactory
from ProductOwner import ProductOwner
"""
    @Name: ProductOwnerFactory
    @Author: Michael Bolles
"""


class ProductOwnerFactory(PawnFactory):

    # Factory method to create a Product Owner Pawn
    @staticmethod
    def create_pawn(player_id):
        return ProductOwner(player_id)
