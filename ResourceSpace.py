from GameSpace import GameSpace
from ProductOwner import ProductOwner
from ScrumMaster import ScrumMaster
import random


class ResourceSpace(GameSpace):
    resourceType: object
    divisor: object

    def __init__(self, resourceType, divisor):
        super().__init__()
        self.resourceType = resourceType
        self.divisor = divisor

    def canPlace(self, list_pawns):
        for pawn in list_pawns:
            if isinstance(pawn, ProductOwner):
                return False
        if len(list_pawns) == 1 and isinstance(list_pawns[0], ScrumMaster):
            return False
        if len(list_pawns) + len(self.pawns) > 7:
            return False
        return True

    def collectPawns(self, player_num):
        GameSpace.DieResult = 0
        GameSpace.SpaceUsed = self
        for pawn in self.pawns:
            if pawn.player == player_num:
                GameSpace.DieResult += random.randint(1, 6)
        self.pawns = [pawn for pawn in self.pawns if pawn.player != player_num]