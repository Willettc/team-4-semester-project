from Requirements import Requirements
import random
import FunctionalRequirement
import NonfunctionalRequirement
from DeveloperFactory import DeveloperFactory
from ScrumMasterFactory import ScrumMasterFactory
from ProductOwnerFactory import ProductOwnerFactory
from Developer import Developer

"""
    @Name: Player Class
    @Description: Stores player information
    @Author: Simon Cichonski
"""


class Player:

    def __init__(self, player_id):
        self.player_id = player_id

        # Creates initial list of pawns
        self.pawn_list = []

        self.pawn_list.append(ProductOwnerFactory.create_pawn(self.player_id))
        self.pawn_list.append(ScrumMasterFactory.create_pawn(self.player_id))

        for x in range(4):
            self.pawn_list.append(DeveloperFactory.create_pawn(self.player_id))

        self.coffee_level = int()

        self.functional_req = []
        self.nonfunctional_req = []
        # Generate undefined resource variables
        self.design = int()
        self.testing = int()
        self.implementation = int()
        self.documentation = int(0)
        self.coffee = int()
        self.tools = int()
        self.cards = []
        self.knowledge = int()
        # Knowledge used during the current round
        self.knowledge_used = int()
        # Generate empty points
        self.points = int()

    # Add pawn to pawn_list
    def add_pawn(self):
        pawn = DeveloperFactory.create_pawn(self.player_id)
        self.pawn_list.append(pawn)

    # Delete newest Pawn from pawn_list
    def del_pawn(self):
        self.pawn_list.remove(self.pawn_list[len(self.pawn_list) - 1])

    def get_pawn(self, pawn_location):
        return self.pawn_list[pawn_location]

    def get_pawn_list(self):
        return self.pawn_list

    # Add or delete resources
    def mod_design(self, number):
        self.design += number

    def mod_testing(self, number):
        self.testing += number

    def mod_implementation(self, number):
        self.implementation += number

    def mod_documentation(self, number):
        self.documentation += number

    def mod_coffee(self, number):
        self.coffee += number

    def mod_knowledge(self, number):
        self.knowledge += number

    def mod_knowledge_used(self, number):
        self.knowledge_used += number

    def mod_coffee_level(self, number):
        self.coffee_level += number

    """
        Returns boolean true if card is obtainable
        @Params: the class, the id of the card in the json file, an instance of the Requirements class
    """

    def is_obtainable_functional(self, card_id, requirements_deck):
        # Get the card first
        card = None  # An initialized card, although empty
        for x in range(len(requirements_deck.functionalRequirements)):
            if int(card_id) == int(requirements_deck.functionalRequirements[x].id):
                card = requirements_deck.functionalRequirements[x]
        # NOTE: ADD IF CARD IS NOT FOUND ERROR LOG
        if card is None:
            return False
        # When the card's resource is too big for the player's resource, return false
        if int(card.designReq) > self.design:
            return False
        elif int(card.testReq) > self.testing:
            return False
        elif int(card.docReq) > self.documentation:
            return False
        elif int(card.impReq) > self.implementation:
            return False
        else:
            return True

    """
        Returns boolean true if card is obtainable
        @Params: the class, the id of the card in the json file, an instance of the Requirements class
    """

    def is_obtainable_nonfunctional(self, card_id, requirements_deck):
        # Get the card first
        card = None  # An uninitialized Python variable
        for x in range(len(requirements_deck.nonfunctionalRequirements)):
            if int(card_id) == int(requirements_deck.nonfunctionalRequirements[x].id):
                card = requirements_deck.nonfunctionalRequirements[x]
        # NOTE: ADD IF CARD IS NOT FOUND ERROR LOG
        if card is None:
            return False
        if int(card.designReq) > self.design:
            return False
        elif int(card.testReq) > self.testing:
            return False
        elif int(card.docReq) > self.documentation:
            return False
        elif int(card.impReq) > self.implementation:
            return False
        else:
            return True

    """
        Add the card to the player's deck, and delete the card from the pile (both displayed pile, and hidden pile)
        @Params: the class, the id of the card in the json file, an instance of the Requirements class
    """

    def add_functional_card(self, card_id, requirements_deck):
        # NOTE: ADD CARD NOT FOUND ERROR
        for x in range(len(requirements_deck.functionalRequirements)):
            if card_id == requirements_deck.functionalRequirements[x].id:
                # Add the card to Player's cards
                functional_requirement = FunctionalRequirement.Functional(
                    requirements_deck.functionalRequirements[x].id,
                    requirements_deck.functionalRequirements[x].name,
                    requirements_deck.functionalRequirements[x].designReq,
                    requirements_deck.functionalRequirements[x].testReq,
                    requirements_deck.functionalRequirements[x].impReq,
                    requirements_deck.functionalRequirements[x].docReq,
                    requirements_deck.functionalRequirements[x].points
                    )
                self.cards.append(functional_requirement)
        # Delete the card
        Requirements.delete_functional_card(requirements_deck, card_id)

    def add_nonfunctional_card(self, card_id, requirements_deck):
        # NOTE: ADD CARD NOT FOUND ERROR
        for x in range(len(requirements_deck.nonfunctionalRequirements)):
            if card_id == requirements_deck.nonfunctionalRequirements[x].id:
                # Add the card to Player's cards
                nonfunctional_requirement = NonfunctionalRequirement.Nonfunctional(
                    requirements_deck.nonfunctionalRequirements[x].id,
                    requirements_deck.nonfunctionalRequirements[x].name,
                    requirements_deck.nonfunctionalRequirements[x].designReq,
                    requirements_deck.nonfunctionalRequirements[x].testReq,
                    requirements_deck.nonfunctionalRequirements[x].impReq,
                    requirements_deck.nonfunctionalRequirements[x].docReq,
                    requirements_deck.nonfunctionalRequirements[x].points,
                    requirements_deck.nonfunctionalRequirements[x].resource,
                    requirements_deck.nonfunctionalRequirements[x].numResource
                    )

                self.cards.append(nonfunctional_requirement)
        # Delete the card
        Requirements.delete_nonfunctional_card(requirements_deck, card_id)

    # TESTING: Display player cards
    def display_deck(self):
        print("Player Cards:")
        for x in range(len(self.cards)):
            print("ID: " + self.cards[x].id)
            print("Name: " + self.cards[x].name)
            print("Design: " + self.cards[x].designReq)
            print("Testing: " + self.cards[x].testReq)
            print("Implementation: " + self.cards[x].impReq)
            print("Documentation: " + self.cards[x].docReq)
            print("Points " + self.cards[x].points)

    def to_string(self):
        print("Player " + str(self.player_id + 1) + "'s Resources:")
        print()
        print("DESIGN: " + str(self.design))
        print("TESTING: " + str(self.testing))
        print("DOCUMENTATION: " + str(self.documentation))
        print("IMPLEMENTATION: " + str(self.implementation))
        print("COFFEE: " + str(self.coffee))
        print("KNOWLEDGE: " + str(self.knowledge - self.knowledge_used))
        print("DEVELOPERS: " + str(len(self.pawn_list) - 2))
        print()

    """
            @Name: display_pawns
            @Description: Displays pawns in a human readable way
            @Author: Michael Bolles
        """
    def display_pawns(self):
        for pawn in self.pawn_list:
            print(pawn)
    """
        @Name: idle_developers
        @Description: Returns an integer list of the index location of pawns that are available for placement
        @Author: Michael Bolles
    """
    def idle_developers(self):
        developers_idle = []
        pawn_index = 0
        for pawn in self.pawn_list:
            if pawn.is_available() and isinstance(pawn, Developer):
                developers_idle.append(pawn_index)
            pawn_index += 1
        return developers_idle


"""
# Object tests

player = Player(0)
requirements = Requirements()

player.mod_design(3)
player.mod_testing(4)
player.mod_documentation(3)
player.mod_implementation(4)
player.mod_coffee(15)
player.mod_tools(5)

print("Player Resources:")
print()
print("DESIGN: " + str(player.design))
print("TESTING: " + str(player.testing))
print("DOCUMENTATION: " + str(player.documentation))
print("IMPLEMENTATION: " + str(player.implementation))
print("COFFEE: " + str(player.coffee))
print("TOOLS: " + str(player.tools))
print()
"""

"""
Requirements.populate_requirements(requirements)
#random.shuffle(requirements.functionalRequirements)
#random.shuffle(requirements.nonfunctionalRequirements)
# Requirements.display_deck(requirements)

# Working Testing Code. Will provide true/false when playing with resource numbers
# print(str(player.is_obtainable_functional("01", requirements)))
# print(str(player.is_obtainable_nonfunctional("01", requirements)))

# Add a card to player if the card is obtainable
if player.is_obtainable_functional("01", requirements):
    player.add_functional_card("01", requirements)

if player.is_obtainable_nonfunctional("01", requirements):
    player.add_nonfunctional_card("01", requirements)

print()
print("ADDED PLAYER CARDS")
player.display_deck()
print()

print()
print("DELETED CARDS FROM DECK")
requirements.display_deck()
print()

"""
