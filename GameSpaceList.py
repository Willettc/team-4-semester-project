from RecruitSpace import RecruitSpace
from CoffeeMachineSpace import CoffeeMachineSpace
from SearchBarSpace import SearchBarSpace
from DocumentationSpace import DocumentationSpace
from DesignSpace import DesignSpace
from ImplementationSpace import ImplementationSpace
from TestingSpace import TestingSpace
from NotStarbucks import NotStarbucks
from NonfunctionalRequirementSpace import NonfunctionalRequirementSpace
from FunctionalRequirementSpace import FunctionalRequirementSpace
from PlayerList import PlayerList


class GameSpaceList:
    GameSpaces = []

    @staticmethod
    def init():
        GameSpaceList.GameSpaces.clear()
        GameSpaceList.GameSpaces.append(RecruitSpace())
        GameSpaceList.GameSpaces.append(CoffeeMachineSpace())
        GameSpaceList.GameSpaces.append(SearchBarSpace())
        GameSpaceList.GameSpaces.append(DocumentationSpace())
        GameSpaceList.GameSpaces.append(DesignSpace())
        GameSpaceList.GameSpaces.append(ImplementationSpace())
        GameSpaceList.GameSpaces.append(TestingSpace())
        GameSpaceList.GameSpaces.append(NotStarbucks())
        for i in range(4):
            GameSpaceList.GameSpaces.append(NonfunctionalRequirementSpace())
        for player in PlayerList.players:
            GameSpaceList.GameSpaces.append(FunctionalRequirementSpace())

    @staticmethod
    def canPlace(index, list_pawns):
        return GameSpaceList.GameSpaces[index].canPlace(list_pawns)

    @staticmethod
    def addPawns(index, list_pawns):
        GameSpaceList.GameSpaces[index].addPawns(list_pawns)

    @staticmethod
    def canCollectPawns(index, player_num):
        return GameSpaceList.GameSpaces[index].canCollectPawns(player_num)

    @staticmethod
    def collectPawns(index, player_num):
        GameSpaceList.GameSpaces[index].collectPawns(player_num)

    @staticmethod
    def displayPlacedPawns():
        for i in range(0, len(GameSpaceList.GameSpaces) - 1):
            print("Space {num}:".format(num=i))
            for pawn in GameSpaceList.GameSpaces[i].pawns:
                print("{type} belonging to player {playernum}".format(type=type(pawn), playernum=pawn.Owner))
