from GameSpace import GameSpace
from ProductOwner import ProductOwner
from PlayerList import PlayerList


class SearchBarSpace(GameSpace):
    def canPlace(self, list_pawns):
        if len(self.pawns) != 0:
            return False
        if len(list_pawns) != 1:
            return False
        if isinstance(list_pawns[0], ProductOwner):
            return False
        return True

    def collectPawns(self, player_num):
        PlayerList.get_player(player_num).knowledge += 1
        self.pawns.clear()
