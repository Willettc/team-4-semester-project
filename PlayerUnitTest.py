import unittest
from Requirements import Requirements
import random
import FunctionalRequirement
import NonfunctionalRequirement
from Player import Player
from DeveloperFactory import DeveloperFactory
from ScrumMasterFactory import ScrumMasterFactory
from ProductOwnerFactory import ProductOwnerFactory


class PlayerUnitTest(unittest.TestCase):

    def test_Player_init(self):
        # Player should be able to be initialized and have 6 pawns automatically
        player_test = Player(1)
        self.assertEqual(len(player_test.pawn_list), 6)

    def test_Player_functionalRequirements(self):
        player = Player(0)
        deck = Requirements()
        Requirements.populate_requirements(deck)

        player.is_obtainable_functional("01", deck)
        player.add_functional_card("01", deck)

        self.assertNotEqual(player.cards, [])

    def test_Player_nonFunctionalRequirements(self):
        player = Player(0)
        deck = Requirements()
        Requirements.populate_requirements(deck)

        player.is_obtainable_nonfunctional("01", deck)
        player.add_nonfunctional_card("01", deck)

        self.assertNotEqual(player.cards, [])

    def test_Player_Resources(self):
        player = Player(0)

        # adding stuff since factory isn't implemented
        player.mod_design(10)  # these modifications occur inside of Gamespace.py at the end of a round
        player.mod_testing(3)
        player.mod_documentation(5)
        player.mod_implementation(6)
        player.mod_coffee(15)
        player.mod_knowledge(5)

        self.assertEqual(player.design, 10)
        self.assertEqual(player.testing, 3)
        self.assertEqual(player.documentation, 5)
        self.assertEqual(player.implementation, 6)
        self.assertEqual(player.coffee, 15)
        self.assertEqual(player.knowledge, 5)

        player.mod_design(-3)
        player.mod_testing(-3)
        player.mod_documentation(-3)
        player.mod_implementation(-3)
        player.mod_coffee(-3)
        player.mod_knowledge(-3)

        self.assertEqual(player.design, 7)
        self.assertEqual(player.testing, 0)
        self.assertEqual(player.documentation, 2)
        self.assertEqual(player.implementation, 3)
        self.assertEqual(player.coffee, 12)
        self.assertEqual(player.knowledge, 2)

    def test_Player_Pawns(self):
        player = Player(0)
        player.add_pawn()    # player automatically starts with 6 pawns
        self.assertEqual(len(player.pawn_list), 7)
        player.del_pawn()
        player.del_pawn()
        self.assertEqual(len(player.pawn_list), 5)

if __name__ == '__main__':
    unittest.main()