class Functional:
    # Constructor method with functional requirement variables
    def __init__(self, id, name, designreq, testreq, impreq, docreq, points):
        self.id = id
        self.name = name
        self.designReq = designreq
        self.testReq = testreq
        self.impReq = impreq
        self.docReq = docreq
        self.points = points