import unittest
from GameInstance import GameInstance
from GameSpaceList import GameSpaceList
from PlayerList import PlayerList
from Requirements import Requirements


# Note, these imports are only required for testing, outside of this only GameInstance needs to be imported to work


class GameInstanceTestCase(unittest.TestCase):
    player_count = 4
    # Game Instance is static and needs to be declared outside of methods that aren't initializers
    GameInstance(player_count)
    GameInstance.current_player = 1
    GameInstance.current_phase = 3

    def test_GameInstance_initialization(self):
        # Game Instance should initialize PlayerList, GameSpaceList and Requirements when it is initialized
        # Showing they were initialized
        print(PlayerList.players)
        print(GameInstance.requirements)
        print(GameSpaceList.GameSpaces)

        self.assertEqual(len(PlayerList.players), self.player_count)
        self.assertNotEqual(GameInstance.requirements, [])
        self.assertNotEqual(GameSpaceList, [])

    def test_GameInstance_getters(self):
        # Each getter should return the object or variable in question
        # Get the ID of the current player
        self.assertEqual(GameInstance.get_current_player_id(), GameInstance.current_player)
        # Get the current Phase
        self.assertEqual(GameInstance.get_current_phase(), GameInstance.current_phase)
        # Get the player object from PlayerList where player_id = current_player
        self.assertEqual(GameInstance.get_current_player(), PlayerList.get_player(GameInstance.current_player))
        # Get the list of players from PlayerList
        self.assertEqual(GameInstance.get_player_list(), PlayerList.players)

    def test_GameInstance_setters(self):
        # Each Setter should set the related variable equal to the input
        # Set the current player to 4
        GameInstance.set_current_player(4)
        self.assertEqual(GameInstance.get_current_player_id(), 4)
        # Set the current phase to 2
        GameInstance.set_current_phase(2)
        self.assertEqual(GameInstance.get_current_phase(), 2)

    # This method cannot be automated without use of additional packages
    def test_GameInstance_place_pawn(self):
        # Place pawn should take the ID of a game space, and a list of pawns and call the addPawns function of
        # GameSpaceList and should set pawns placement status to true
        # Place pawn on the coffee machine, which only requires one pawn

        list_pawns = [PlayerList.get_player(0).get_pawn(3)]
        # Removing confirmation
        GameInstance.place_pawn(1, list_pawns)
        # Checking if the pawn was placed correctly
        self.assertEqual(all(item in GameSpaceList.GameSpaces[1].pawns for item in list_pawns), True)
        self.assertEqual(list_pawns[0].placed, True)

    def test_GameInstance_first_developer(self):
        # select_first_developer should return the first available developer from the list of indices
        developer = GameInstance.select_first_developer(0)
        self.assertEqual(developer, PlayerList.get_player(0).idle_developers()[0])

    def test_GameInstance_feed_pawn(self):
        """
        Feed pawn should feed the inputted pawn, if the player does not have enough coffee it sets it status to false
        If the pawn is already idle I.E. was unavailable to be placed the current round, it should set its status to
        true. After this, it should reset the pawn's placement status to unplaced
        """
        # Pawn is properly fed
        player = GameInstance.get_player_list()[0]
        player.mod_coffee(1)
        pawn = player.get_pawn(0)
        pawn.placed = True
        pawn.status = False
        GameInstance.feed_pawn(pawn)
        self.assertEqual(pawn.placed, False)
        self.assertEqual(pawn.status, True)
        self.assertEqual(player.coffee, 0)

        # Player did not have enough coffee
        pawn.placed = True
        pawn.status = False
        GameInstance.feed_pawn(pawn)
        self.assertEqual(pawn.placed, False)
        self.assertEqual(pawn.status, False)
        self.assertEqual(player.coffee, 0)

        # Pawn was unavailable and not placed
        pawn.placed = False
        pawn.status = False
        GameInstance.feed_pawn(pawn)
        self.assertEqual(pawn.placed, False)
        self.assertEqual(pawn.status, True)
        self.assertEqual(player.coffee, 0)

    # This method cannot be automated without use of additional packages
    # def test_GameInstance_adjust_die(self):
    #     """
    #         adjust_die should ask the user if they want to use knowledge to improve a die roll, and return the result
    #         The player should not be able to spend more knowledge than they have
    #         It should also validate the input to make sure that
    #     """
    #     # This function is normally called by reclaim pawn, so input is faked
    #     # Inputs are: the rolled value from game spaces, the resource type, the divider and the player id
    #     # Player correctly inputs knowledge to use, so an input between 1-9
    #     player = GameInstance.get_current_player()
    #     player.mod_knowledge(10)
    #     print(' ENTER 2 FOR UNIT TEST')
    #     die_result = GameInstance.adjust_die(4, "Documentation", 3, 1)
    #     self.assertEqual(die_result, 6)
    #     # Player attempts to input a letter
    #     print(' ENTER A LETTER FOR UNIT TEST, THEN AN INTEGER TO QUIT BETWEEN 0 AND 10')
    #     die_result = GameInstance.adjust_die(4, "Documentation", 3, 1)
    #
    #     # Player attempts to enter a number greater than 10
    #     print(' ENTER 11 FOR UNIT TEST, 0 TO QUIT')
    #     die_result = GameInstance.adjust_die(4, "Documentation", 3, 1)
    #
    #     # Player attempts to enter a number less than 0
    #     print(' ENTER -1 or lower FOR UNIT TEST, 0 TO QUIT')
    #     die_result = GameInstance.adjust_die(4, "Documentation", 3, 1)

    def test_GameInstance_reclaim_pawn(self):
        """
            Reclaim pawn should reclaim each pawn and awards the player with appropriate resources
            Despite the name, reclaim pawns only deals with resource obtaining due to difficulties feeding pawns in it
        """
        # Fake pawn placement
        player = GameInstance.get_current_player()
        player.knowledge = 0
        player.knowledge_used = 0
        # Game spaces requires an array of pawns
        # Placing pawns to guarantee that at least 1 coffee is obtained because RNG
        pawn_list = [player.get_pawn(player.idle_developers()[0]),
                     player.get_pawn(player.idle_developers()[1])]
        GameInstance.place_pawn(7, pawn_list)
        for pawn in pawn_list:
            pawn.location = "SunDoes"
            pawn.location_id = 7
        GameInstance.reclaim_pawn()
        self.assertGreater(player.coffee, 0)
        """
            Due to the RNG nature of the game, the other resources are not tested here, but the code is identical to 
            coffee aside from what the divider is. 
            The functionality of the other spaces should be tested in GameSpaceList as all reclaim_pawn does for this is
            call the collectPawns method
        """

    def test_GameInstance_end_round(self):
        # End round calls reclaim pawn, gives the player coffee based on their coffee machine level then feeds pawns
        player = GameInstance.get_current_player()
        player.mod_coffee_level(10)
        pawn = player.get_pawn(0)
        pawn.placed = True
        pawn.status = False
        GameInstance.end_round()
        print(player.coffee)
        self.assertEqual(pawn.placed, False)
        self.assertEqual(pawn.status, True)
        self.assertEqual(int(player.coffee), 9)


if __name__ == '__main__':
    unittest.main()
