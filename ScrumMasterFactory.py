from PawnFactory import PawnFactory
from ScrumMaster import ScrumMaster
"""
    @Name: ScrumMasterFactory
    @Author: Michael Bolles
"""


class ScrumMasterFactory(PawnFactory):

    # Factory method to create a Scrum Master Pawn
    @staticmethod
    def create_pawn(player_id):
        return ScrumMaster(player_id)

