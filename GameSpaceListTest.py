from unittest import TestCase
from GameInstance import GameInstance
from GameSpaceList import GameSpaceList
from GameSpace import GameSpace
from PlayerList import PlayerList


class TestGameSpaceList(TestCase):
    def test_init(self):
        GameInstance.__init__(4)
        self.assertEqual(len(GameSpaceList.GameSpaces), 16)

    def test_can_place(self):
        GameInstance.__init__(4)

        # generate the pawn lists we'll be using to test the spaces
        pawns_PO_SM = PlayerList.get_player(0).pawn_list[0:2]
        pawns_PO_Dev = [PlayerList.get_player(0).pawn_list[0], PlayerList.get_player(0).pawn_list[2]]
        pawns_SM_Dev = PlayerList.get_player(0).pawn_list[1:3]
        pawns_Dev = [PlayerList.get_player(0).pawn_list[2]]
        pawns_PO = [PlayerList.get_player(0).pawn_list[0]]
        pawns_SM = [PlayerList.get_player(0).pawn_list[1]]
        pawns_Dev_Dev = PlayerList.get_player(0).pawn_list[2:4]
        pawns_Dev_Dev_Dev = PlayerList.get_player(0).pawn_list[2:5]

        pawns_fill_resource = PlayerList.get_player(1).pawn_list[1:7] # To fill up the resource spaces
        pawns_fill_requirement = [PlayerList.get_player(1).pawn_list[0], PlayerList.get_player(1).pawn_list[3]] # To fill a requirement space
        pawns_fill_recruit = PlayerList.get_player(1).pawn_list[2:4] # To fill recruit space
        pawns_fill_other = [PlayerList.get_player(1).pawn_list[3]] # To fill the other assorted spaces

        # Test Recruit Space
        self.assertFalse(GameSpaceList.canPlace(0, pawns_PO_SM))
        self.assertTrue(GameSpaceList.canPlace(0, pawns_PO_Dev))
        self.assertFalse(GameSpaceList.canPlace(0, pawns_SM_Dev))
        self.assertFalse(GameSpaceList.canPlace(0, pawns_Dev))
        self.assertFalse(GameSpaceList.canPlace(0, pawns_PO))
        self.assertFalse(GameSpaceList.canPlace(0, pawns_SM))
        self.assertTrue(GameSpaceList.canPlace(0, pawns_Dev_Dev))
        self.assertFalse(GameSpaceList.canPlace(0, pawns_Dev_Dev_Dev))

        GameSpaceList.addPawns(0, pawns_fill_recruit)
        self.assertFalse(GameSpaceList.canPlace(0, pawns_PO_Dev))

        # Test Coffee Machine Upgrade Space
        self.assertFalse(GameSpaceList.canPlace(1, pawns_PO_SM))
        self.assertFalse(GameSpaceList.canPlace(1, pawns_PO_Dev))
        self.assertFalse(GameSpaceList.canPlace(1, pawns_SM_Dev))
        self.assertTrue(GameSpaceList.canPlace(1, pawns_Dev))
        self.assertFalse(GameSpaceList.canPlace(1, pawns_PO))
        self.assertFalse(GameSpaceList.canPlace(1, pawns_SM))
        self.assertFalse(GameSpaceList.canPlace(1, pawns_Dev_Dev))
        self.assertFalse(GameSpaceList.canPlace(1, pawns_Dev_Dev_Dev))

        GameSpaceList.addPawns(1, pawns_fill_other)
        self.assertFalse(GameSpaceList.canPlace(1, pawns_Dev))

        # Test Search Bar Upgrade Space
        self.assertFalse(GameSpaceList.canPlace(2, pawns_PO_SM))
        self.assertFalse(GameSpaceList.canPlace(2, pawns_PO_Dev))
        self.assertFalse(GameSpaceList.canPlace(2, pawns_SM_Dev))
        self.assertTrue(GameSpaceList.canPlace(2, pawns_Dev))
        self.assertFalse(GameSpaceList.canPlace(2, pawns_PO))
        self.assertTrue(GameSpaceList.canPlace(2, pawns_SM))
        self.assertFalse(GameSpaceList.canPlace(2, pawns_Dev_Dev))
        self.assertFalse(GameSpaceList.canPlace(2, pawns_Dev_Dev_Dev))

        GameSpaceList.addPawns(2, pawns_fill_other)
        self.assertFalse(GameSpaceList.canPlace(2, pawns_Dev))

        # Test a limited resource space (they all use the same function)
        self.assertFalse(GameSpaceList.canPlace(3, pawns_PO_SM))
        self.assertFalse(GameSpaceList.canPlace(3, pawns_PO_Dev))
        self.assertTrue(GameSpaceList.canPlace(3, pawns_SM_Dev))
        self.assertTrue(GameSpaceList.canPlace(3, pawns_Dev))
        self.assertFalse(GameSpaceList.canPlace(3, pawns_PO))
        self.assertFalse(GameSpaceList.canPlace(3, pawns_SM))
        self.assertTrue(GameSpaceList.canPlace(3, pawns_Dev_Dev))
        self.assertTrue(GameSpaceList.canPlace(3, pawns_Dev_Dev_Dev))

        GameSpaceList.addPawns(3, pawns_fill_resource)
        self.assertFalse(GameSpaceList.canPlace(3, pawns_Dev_Dev_Dev))
        self.assertTrue(GameSpaceList.canPlace(3, pawns_Dev_Dev))

        # Test Not Starbucks
        self.assertFalse(GameSpaceList.canPlace(7, pawns_PO_SM))
        self.assertFalse(GameSpaceList.canPlace(7, pawns_PO_Dev))
        self.assertFalse(GameSpaceList.canPlace(7, pawns_SM_Dev))
        self.assertTrue(GameSpaceList.canPlace(7, pawns_Dev))
        self.assertFalse(GameSpaceList.canPlace(7, pawns_PO))
        self.assertFalse(GameSpaceList.canPlace(7, pawns_SM))
        self.assertTrue(GameSpaceList.canPlace(7, pawns_Dev_Dev))
        self.assertTrue(GameSpaceList.canPlace(7, pawns_Dev_Dev_Dev))

        # Test a requirement space (they all use the same function)
        self.assertFalse(GameSpaceList.canPlace(8, pawns_PO_SM))
        self.assertTrue(GameSpaceList.canPlace(8, pawns_PO_Dev))
        self.assertFalse(GameSpaceList.canPlace(8, pawns_SM_Dev))
        self.assertFalse(GameSpaceList.canPlace(8, pawns_Dev))
        self.assertFalse(GameSpaceList.canPlace(8, pawns_PO))
        self.assertFalse(GameSpaceList.canPlace(8, pawns_SM))
        self.assertFalse(GameSpaceList.canPlace(8, pawns_Dev_Dev))
        self.assertFalse(GameSpaceList.canPlace(8, pawns_Dev_Dev_Dev))

        GameSpaceList.addPawns(8, pawns_fill_requirement)
        self.assertFalse(GameSpaceList.canPlace(8, pawns_PO_Dev))



    def test_add_pawns(self):
        GameInstance.__init__(4)

        # generate the pawn lists we'll be using to test the spaces
        pawns_fill_resource = PlayerList.get_player(1).pawn_list[1:7]
        pawns_fill_requirement = [PlayerList.get_player(1).pawn_list[0],
                                  PlayerList.get_player(1).pawn_list[3]]
        pawns_fill_recruit = PlayerList.get_player(1).pawn_list[2:4]
        pawns_fill_other = [PlayerList.get_player(1).pawn_list[3]]

        # Because pawns aren't marked as being placed in the add functions, we can reuse the lists here
        GameSpaceList.addPawns(0, pawns_fill_recruit)
        self.assertEqual(len(GameSpaceList.GameSpaces[0].pawns), 2)

        GameSpaceList.addPawns(1, pawns_fill_other)
        self.assertEqual(len(GameSpaceList.GameSpaces[1].pawns), 1)

        GameSpaceList.addPawns(2, pawns_fill_other)
        self.assertEqual(len(GameSpaceList.GameSpaces[2].pawns), 1)

        GameSpaceList.addPawns(3, pawns_fill_resource)
        self.assertEqual(len(GameSpaceList.GameSpaces[3].pawns), 5)

        GameSpaceList.addPawns(7, pawns_fill_recruit)
        self.assertEqual(len(GameSpaceList.GameSpaces[7].pawns), 2)

        GameSpaceList.addPawns(8, pawns_fill_requirement)
        self.assertEqual(len(GameSpaceList.GameSpaces[8].pawns), 2)

    def test_can_collect_pawns(self):
        GameInstance.__init__(4)
        # All spaces use the same function for this, it's just a simple "Are there any pawns that match owner to current player"
        pawns_plr0 = PlayerList.get_player(0).pawn_list[2:4]

        GameSpaceList.addPawns(3, pawns_plr0)
        self.assertTrue(GameSpaceList.canCollectPawns(3, 0))
        self.assertFalse(GameSpaceList.canCollectPawns(3, 1))


    def test_collect_pawns(self):
        GameInstance.__init__(4)

        # generate the pawn lists we'll be using to test the spaces
        pawns_fill_resource = PlayerList.get_player(1).pawn_list[1:7]
        pawns_fill_requirement = [PlayerList.get_player(1).pawn_list[0],
                                  PlayerList.get_player(1).pawn_list[3]]
        pawns_fill_recruit = PlayerList.get_player(1).pawn_list[2:4]
        pawns_fill_other = [PlayerList.get_player(1).pawn_list[3]]

        # Because pawns aren't marked as being placed in the add functions, we can reuse the lists here
        GameSpaceList.addPawns(0, pawns_fill_recruit)
        GameSpaceList.addPawns(1, pawns_fill_other)
        GameSpaceList.addPawns(2, pawns_fill_other)
        GameSpaceList.addPawns(3, pawns_fill_resource)
        GameSpaceList.addPawns(8, pawns_fill_requirement)

        # Recruit Space
        currNumPawns = len(PlayerList.get_player(1).pawn_list)
        GameSpaceList.collectPawns(0, 1)
        self.assertEqual(len(GameSpaceList.GameSpaces[0].pawns), 0)                 # See if space is emptied
        self.assertEqual(currNumPawns + 1, len(PlayerList.get_player(1).pawn_list)) # See if pawn was added

        # Coffee Machine Space
        currLevel = PlayerList.get_player(1).coffee_level
        GameSpaceList.collectPawns(1, 1)
        self.assertEqual(len(GameSpaceList.GameSpaces[1].pawns), 0)  # See if space is emptied
        self.assertEqual(currLevel + 1, PlayerList.get_player(1).coffee_level)  # See if level increased

        # Search Bar Space
        currLevel = PlayerList.get_player(1).knowledge
        GameSpaceList.collectPawns(2, 1)
        self.assertEqual(len(GameSpaceList.GameSpaces[2].pawns), 0)  # See if space is emptied
        self.assertEqual(currLevel + 1, PlayerList.get_player(1).knowledge)  # See if level increased

        # Resource Space (These are all the same, including Not Starbucks)
        GameSpace.DieResult = -1
        GameSpace.SpaceUsed = None # clear these to make sure they're edited properly
        GameSpaceList.collectPawns(3, 1)
        self.assertEqual(len(GameSpaceList.GameSpaces[3].pawns), 0)  # See if space is emptied
        self.assertNotEqual(GameSpace.DieResult, -1) # We don't actually know what it will have, but it should be a number higher than -1
        self.assertEqual(GameSpace.SpaceUsed, GameSpaceList.GameSpaces[3])

        # Requirement Space (This one is a bit trickier)
        GameSpaceList.GameSpaces[8].card = GameInstance.requirements.nonfunctionalRequirements[0]
        GameSpaceList.collectPawns(8, 1)
        self.assertEqual(len(GameSpaceList.GameSpaces[8].pawns), 0)  # See if space is emptied
        self.assertIsNotNone(GameSpaceList.GameSpaces[8].card)      # Player does not have adequate resources to claim
        GameSpaceList.addPawns(8, pawns_fill_requirement)
        PlayerList.get_player(1).design = 999
        PlayerList.get_player(1).testing = 999
        PlayerList.get_player(1).implementation = 999
        PlayerList.get_player(1).documentation = 999
        GameSpaceList.collectPawns(8, 1)
        self.assertIsNone(GameSpaceList.GameSpaces[8].card)      # Card should now be claimed
        self.assertEqual(PlayerList.get_player(1).nonfunctional_req[0], GameInstance.requirements.nonfunctionalRequirements[0])


    def test_display_placed_pawns(self):
        pass # We don't actually care about this function
